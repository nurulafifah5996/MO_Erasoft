﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MO_Erasoft.Controllers;
using System.Net.Http;
using System.Web.Http;

using Hangfire;
using Hangfire.Server;
using Hangfire.Storage;
using Hangfire.SqlServer;
using MO_Erasoft.Models;

namespace MO_Erasoft.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            AKUN_ERA_MODbContext = new AKUN_ERA_MODbContext("");
        }
        public ActionResult Index()
        {
            //CallHangfireStok();
            return View();
        }
        public AKUN_ERA_MODbContext AKUN_ERA_MODbContext { get; set; }
        public void CallHangfireStok()
        {
            try
            {
                var AKUN_Object = AKUN_ERA_MODbContext.Database.SqlQuery<AKUN>("SELECT * FROM AKUN NOLOCK WHERE ISNULL(STATUS_WEBHOOK,'')='1'").ToList();
                if (AKUN_Object.Count() > 0)
                {
                    foreach (var db in AKUN_Object)
                    {
                        var CS_API = db.APIConnectionString;
                        var ServerAPI = "";
                        var DatabaseAPI = "";

                        int indexAPI = CS_API.IndexOf(';');
                        if (indexAPI > 0)
                        {
                            ServerAPI = CS_API.Substring(0, indexAPI);
                        }

                        ServerAPI = ServerAPI.Substring(12);

                        DatabaseAPI = CS_API.Substring(CS_API.LastIndexOf("=") + 1);
                        
                        var APIDbContextA = new APIDbContext(ServerAPI, DatabaseAPI);

                        //var EDB = new DatabaseSQL(nourut);

                        //string EDBConnID = EDB.GetConnectionString("ConnID");
                        var koneksi = "Data Source=" + ServerAPI + ";Initial Catalog=" + DatabaseAPI + ";Persist Security Info=True;User ID=sa;Password=admin123^";
                        var sqlStorage = new SqlServerStorage(koneksi);

                        var monitoringApi = sqlStorage.GetMonitoringApi();
                        var serverList = monitoringApi.Servers();
                        var optionsStokServer = new BackgroundJobServerOptions
                        {
                            ServerName = "Stok",
                            Queues = new[] { "1_update_stok_mo" },
                            WorkerCount = 2,
                            
                        };
                        var newStokServer = new BackgroundJobServer(optionsStokServer, sqlStorage);

                        var client = new BackgroundJobClient(sqlStorage);
                        RecurringJobManager recurJobM = new RecurringJobManager(sqlStorage);
                        RecurringJobOptions recurJobOpt = new RecurringJobOptions()
                        {
                            QueueName = "1_update_stok_mo",
                        };

                        var connection_id_proses_akhir_tahun = DatabaseAPI + "_prosesMutasiStok";
                        recurJobM.RemoveIfExists(connection_id_proses_akhir_tahun);
                        recurJobM.AddOrUpdate(connection_id_proses_akhir_tahun, Hangfire.Common.Job.FromExpression<ValuesController>(x => x.ProsesMutasiStok(db, DatabaseAPI, ServerAPI)), "*/5 * * * *", recurJobOpt);
                    }
                }
            }catch(Exception ex)
            {

            }
            //return "OK";
        }
    }
}
