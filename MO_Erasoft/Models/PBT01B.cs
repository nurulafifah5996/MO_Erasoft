﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MO_Erasoft.Models
{
    using System;
    using System.Collections.Generic;

    public partial class PBT01B
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public PBT01B()
        //{
        //    this.PBT01E = new HashSet<PBT01E>();
        //}

        public string JENISFORM { get; set; }
        public string INV { get; set; }
        public string PO { get; set; }
        public int NO { get; set; }
        public string BRG { get; set; }
        public string NAMA_BRG { get; set; }
        public string GD { get; set; }
        public string BK { get; set; }
        public double QTY { get; set; }
        public double DISC2 { get; set; }
        public double NDISC2 { get; set; }
        public double HBELI { get; set; }
        public double THARGA { get; set; }
        public string NOBUK { get; set; }
        public string AUTO_LOAD { get; set; }
        public double QTY_RETUR { get; set; }
        public double BIAYA { get; set; }
        public string USERNAME { get; set; }
        public System.DateTime TGLINPUT { get; set; }
        public double TOTAL_LOT { get; set; }
        public double TOTAL_QTY { get; set; }
        public double DISCOUNT_1 { get; set; }
        public double DISCOUNT_2 { get; set; }
        public double DISCOUNT_3 { get; set; }
        public double NILAI_DISC_1 { get; set; }
        public double NILAI_DISC_2 { get; set; }
        public double NILAI_DISC_3 { get; set; }
        public string KET { get; set; }
        public double NO_URUT_PO { get; set; }
        public double PPNBM { get; set; }
        public double NILAI_PPNBM { get; set; }
        public string BRG_ORIGINAL { get; set; }
        public string LKU { get; set; }

        //public virtual PBT01A PBT01A { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<PBT01E> PBT01E { get; set; }
    }
}
