﻿using MO_Erasoft.Models;
using System.Data.Entity;

namespace MO_Erasoft
{
    public class PBDbContext : DbContext
    {
        //public virtual DbSet<SIT01A_MO> SIT01A_MO { get; set; }

        public virtual DbSet<PBT01A> PBT01A { get; set; }

        public virtual DbSet<PBT01B> PBT01B { get; set; }


        public PBDbContext() : base("name=PBDbContext") { }
        public PBDbContext(string dbSourcePB) : base("name=PBDbContext") { }

        public PBDbContext(string dbSourceEra, string dbPathEra)
           : base($"Server={dbSourceEra};initial catalog={dbPathEra};" +
                  $"user id=sa;password=admin123^;multipleactiveresultsets=True;" +
                  $"application name=EntityFramework")
        {
        }

    }
}