﻿using MO_Erasoft.Models;
using System.Data.Entity;

namespace MO_Erasoft
{
    public class APDbContext : DbContext
    {
        //public virtual DbSet<SIT01A_MO> SIT01A_MO { get; set; }

        public virtual DbSet<APF01> APF01 { get; set; }

        public virtual DbSet<APFSY> APFSY { get; set; }


        public APDbContext() : base("name=APDbContext") { }
        public APDbContext(string dbSourceAP) : base("name=APDbContext") { }

        public APDbContext(string dbSourceEra, string dbPathEra)
           : base($"Server={dbSourceEra};initial catalog={dbPathEra};" +
                  $"user id=sa;password=admin123^;multipleactiveresultsets=True;" +
                  $"application name=EntityFramework")
        {
        }

    }
}